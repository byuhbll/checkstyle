# BYU HBLL Checkstyle Rules

> "Checkstyle is a development tool to help programmers write Java code that adheres to a coding standard.  It automates the process of checking Java code to spare humans of this boring (but important) task."

— [Checkstyle Project Overview](http://checkstyle.sourceforge.net/)

----

This repository contains the checkstyle rules for the BYU Harold B. Lee Library (HBLL).

## Using Checkstyle

### Running checkstyle in Maven

You can manually run the checks with the `mvn verify` command after including the following in your POM. Note that if you are using the `java-pom` parent POM, this is already included for you.

```xml
<build>
  <plugins>
    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-checkstyle-plugin</artifactId>
      <version>2.17</version>
      <executions>
        <execution>
          <phase>verify</phase>
          <configuration>
            <configLocation>byuhbll_checks.xml</configLocation>
            <encoding>UTF-8</encoding>
            <consoleOutput>true</consoleOutput>
            <failsOnError>false</failsOnError>
            <linkXRef>false</linkXRef>
          </configuration>
          <goals>
            <goal>check</goal>
          </goals>
        </execution>
      </executions>
    </plugin>
  </plugins>
  <extensions>
    <extension>
      <groupId>edu.byu.hbll</groupId>
      <artifactId>checkstyle</artifactId>
      <version>1.0.0</version>
    </extension>
  </extensions>
</build>
```

### Enabling checkstyle in Eclipse

To enable checkstyle globally and import the BYU HBLL checkstyle configuration:

1. Install the [checkstyle plugin](https://marketplace.eclipse.org/content/checkstyle-plug) from the Eclipse Marketplace (*Help* » *Eclipse Marketplace...*).
2. Open the Eclipse preferences dialog (**Windows/Linux:** *Window* » *Preferences*; **Mac:** *Eclipse* » *Preferences*) and select *Checkstyle* from the navigation pane.
3. Create a new Global Check Configuration by clicking the *New...* button in the checkstyle options screen:
    * Select "Remote Configuration" as the *Type*;
    * Enter "BYU HBLL Checks" as the *Name*;
    * Enter "https://maven.lib.byu.edu/byuhbll_checks.xml" as the *Location*;
    * Ignore other fields/options, and click the *OK* button to create the configuration.
4. Select the newly created "BYU HBLL Checks" configuration and click the *Set as Default* button.

To enable checkstyle (must be done for each project):

1. Right-click on the project in the Project Explorer.
2. In the resulting context menu, select *Checkstyle* » *Activate Checkstyle*.

Alternatively, or if you need to change which checkstyle configuration is being used for a given project, you can access the full checkstyle configuration for each project through the project's *Properties* dialog.

### Enabling checkstyle in IntelliJ IDEA

To enable checkstyle and import the BYU HBLL checkstyle configuration:

1. Open the Settings dialog (*File* » *Settings...*) and select *Plugins* from the navigation pane.
2. Click the *Browse Repositories...* button.
3. Search for and install the "CheckStyle-IDEA" plugin.
4. Restart IntelliJ IDEA if necessary, and re-open the Settings dialog.
5. Select *Other Settings* » *Checkstyle* from the navigation pane.
6. Create a new Configuration File by clicking the **+** button on the right side of the Configuration File list:
    * Enter "BYU HBLL Checks" as the *Description*
    * Select the option to *Use a Checkstyle file accessible via HTTP*;
    * Enter "https://maven.lib.byu.edu/byuhbll_checks.xml" as the *URL*;
    * Click the *OK* button to create the configuration.
7. Select the *Active* checkbox next to the newly created "BYU HBLL Checks" configuration.
8. Select the checkbox to *Treat Checkstyle errors as warnings*.
9. Click *OK* or *Apply* to save your changes.

## Eclipse Formatter
To automate formatting to be in line with the checkstyle, import the `byuhbll_eclipse.xml` file into Eclipse and configure Eclipse to run the formatter on save.

### Import Eclipse Formatter Rules
1. Download `byuhbll_eclipse.xml` from this repository
2. `Window` -> `Preferences` -> `Java` -> `Code Style` -> `Formatter`
3. `Import...` the `byuhbll_eclipse.xml`
4. Verify that `byuhbll` is the active profile.
5. `OK`

These rules can also be used in IntelliJ through the Eclipse Code Formatter plugin.

### Autoformat on Save
1. `Window` -> `Preferences` -> `Java` -> `Editor` -> `Save Actions`
2. Check `Perform the selected actions on save`
3. Check `Format source code`
4. Select `Format all lines`
5. Check `Organize imports`
6. Check `Additional actions`


## Other Ideas

You may wish to configure the formatter of your IDE to help you write checkstyle-compliant code.  Here are some ideas to help make checkstyle a painless but helpful tool to improve your code style:

### Enable a line-width indicator in Eclipse

Eclipse can optionally display a vertical line to assist you in recognizing when you have reached the expected line length.

1. Open the Eclipse preferences dialog (**Windows/Linux:** *Window* » *Preferences*; **Mac:** *Eclipse* » *Preferences*) and select *General* » *Editors* » *Text Editors* from the navigation pane.
2. Click the checkbox to *Show print margin* and enter "120" as the *Print margin column*.
3. Click *OK* to confirm the change.